/*
* Your task is to make a function that can take any non-negative integer as an
* argument and return it with its digits in descending order.
* Essentially, rearrange the digits to create the highest possible number.
* */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
/*
class DescendingOrder {

    public static void main(String[] args) {

        BufferedReader buffReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Type some numbers: ");
            String numbers = buffReader.readLine();
            Integer[] array = new Integer[(numbers.length())];

            for (int i = 0; i < numbers.length(); i++){
                System.out.print(numbers.charAt(i));
                array[i] = Integer.parseInt(String.valueOf(numbers.charAt(i)));
            }
            Arrays.sort(array, Collections.reverseOrder());
            System.out.println("\n" + Arrays.toString(array));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
*/