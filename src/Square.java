public class Square {
    public static boolean isSquare(int n) {
        double doubleN = (double) n;
        if(doubleN < 0) {
            return false;
        }
        double square = Math.sqrt(doubleN);

        if(((square - Math.floor(square)) == 0)){
            return true;
        } else{
            return false;
        }
    }

    public static void main(String[] args) {
        isSquare(25);
        isSquare(-1);
        isSquare(26);
        isSquare(0);
    }
}