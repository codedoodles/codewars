/*
* accum("abcd") -> "A-Bb-Ccc-Dddd"
* accum("RqaEzty") -> "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
* accum("cwAt") -> "C-Ww-Aaa-Tttt"
* */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Accumul {
    private static final String REGEX = "[a-zA-z]+";
    public static String accum(String s) {
        if(!s.matches(REGEX)){
            throw new Error("Not allowed.");
        }
        StringBuilder newString = new StringBuilder();
        char[] character = s.toCharArray();
        for (int i = 0; i < character.length; i++) {
            for (int j = 0; j <= i; j++) {
                if(j == 0){
                    newString.append(Character.toUpperCase(character[i]));
                } else {
                    newString.append(Character.toLowerCase(character[i]));
                }
            }
            newString.append("-");
        }
        newString.deleteCharAt(newString.length()-1);
        String accumulate = newString.toString();
        return accumulate;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String accioLetters= bufferedReader.readLine();
        System.out.println(accum(accioLetters));
    }
}