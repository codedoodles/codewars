import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzTest {
    FizzBuzz fizz = new FizzBuzz();


    @Test
    void fizzThree() throws Exception{
        assertEquals(fizz.FizzBuzzTest(3), "Fizz" );
    }

    @Test
    void fizzFive() throws Exception{
        assertEquals("Buzz" , fizz.FizzBuzzTest(5));
    }

    @Test
    void fizzBuzz() throws Exception{
        assertEquals("FizzBuzz",fizz.FizzBuzzTest(15));
    }

    @Test
    void fizzNrThree() throws Exception{
        assertEquals("Fizz", fizz.FizzBuzzTest(32));
    }

    @Test
    void fizzNrFive() throws Exception{
        assertEquals("Buzz", fizz.FizzBuzzTest(52));
    }

    @Test
    void fizzBuzzThreeFive() throws Exception{
        assertEquals("FizzBuzz", fizz.FizzBuzzTest(534));
    }

    @Test
    void fizzBuzzNumber() throws Exception{
        assertEquals("28", fizz.FizzBuzzTest(28));
        assertEquals("1", fizz.FizzBuzzTest(1));
    }
}