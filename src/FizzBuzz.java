public class FizzBuzz {
    public Boolean isDivisibleOrContains(int n, int c){
        if(n % c == 0){
            return true;
        }
          else if(checkContains(n, c)){
            return true;
        }
        else return false;
    }

    //Can check if number contains c by (modulus) dividing n in to tens and checking the remainder against c.
    public Boolean checkContains(int n, int c){
        int remainder;

        while(n > 0){
            remainder = n % 10;

            if(remainder == c){
                return true;
            }
            n = (n / 10);
        }
        return false;
    }

    public String FizzBuzzTest(int n){
        StringBuilder fizzString = new StringBuilder();

        if(isDivisibleOrContains(n, 3)){
            fizzString.append("Fizz");
        }
        if(isDivisibleOrContains(n, 5)){
            fizzString.append("Buzz");
        }
        if(fizzString.isEmpty()){
            fizzString.append(n);
        }
        return  fizzString.toString();
    }

}
